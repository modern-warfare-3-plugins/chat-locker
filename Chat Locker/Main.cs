﻿using Addon;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;

namespace ChatLocker
{
    public class ChatLocker : CPlugin
    {
        private readonly Dictionary<string, DateTime> _playerLocks = new Dictionary<string, DateTime>();
        private List<string> _immunePlayers;

        /// <summary>
        /// Executes when the MW3 server loads
        /// </summary>
        public override void OnServerLoad()
        {
            ServerPrint("Chat Locker created by SgtLegend. Version 0.3");

            // Get the configuration
            _immunePlayers = new List<string>(GetServerCFG("ChatLocker", "ImmunePlayers", "").Split(','));
        }

        /// <summary>
        /// Executes when a client types something into the chat
        /// </summary>
        /// <param name="message"></param>
        /// <param name="client"></param>
        /// <param name="teamchat"></param>
        /// <returns></returns>
        public override ChatType OnSay(string message, ServerClient client, bool teamchat)
        {
            List<string> cmd = new List<string>(message.Split(' '));
 
            // Check if the current player has been chat locked
            if (!message.StartsWith("!") && _playerLocks.ContainsKey(client.XUID))
            {
                TimeSpan remaining = DateTime.Now - _playerLocks[client.XUID];
 
                // If the time span is greater than the remaining time let the player know they are still chat locked
                if (TimeSpan.Zero > remaining)
                {
                    iPrintLnBold(string.Format(
                        "You are still chat locked for ^:{0} ^7hour(s) ^:{1} ^7minute(s) and ^:{2} ^7second(s)",
                        Math.Abs(remaining.Hours),
                        Math.Abs(remaining.Minutes),
                        Math.Abs(remaining.Seconds)), client);
 
                    return ChatType.ChatNone;
                }
 
                // If the time span is less or equal to the remaining time remove the players current chat lock status
                if (TimeSpan.Zero <= remaining)
                {
                    _playerLocks.Remove(client.XUID);
                }
            }
 
            // Check if the given command is associated with the Chat Locker plugin
            Match matches = Regex.Match(cmd[0], @"\!((?:un|whos)?lock(?:ed)?)c?)", RegexOptions.IgnoreCase);
 
            if (matches.Success)
            {
                string match = matches.Groups[1].Value.ToLower();
                cmd.RemoveAt(0);
 
                switch (match)
                {
                    case "lock":
                    case "lockc":
                        LockPlayerChat(cmd, client, (match == "lockc"));
                        break;
 
                    case "unlock":
                    case "unlockc":
                        UnlockPlayerChat(cmd, client, (match == "unlockc"));
                        break;
 
                    case "whoslocked":
                        FindChatLockedPlayers(client);
                        break;
                }
 
                return ChatType.ChatNone;
            }
 
            return ChatType.ChatContinue;
        }

        /// <summary>
        /// Locks the given players chat for X amount of minutes
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="issuer"></param>
        /// <param name="byClientId"></param>
        private void LockPlayerChat(List<string> cmd, ServerClient issuer, bool byClientId)
        {
            if (cmd.Count == 0 || string.IsNullOrEmpty(cmd[0]))
            {
                TellClient(issuer.ClientNum, "^1[Chat Lock] ^7Please enter a valid player id or name", true);
            }
            else
            {
                double timeToLock = 0;

                if (cmd.Count < 2 || (cmd[1] != null && !double.TryParse(cmd[1], out timeToLock) && timeToLock > 0))
                {
                    TellClient(issuer.ClientNum, "^1[Chat Lock] ^7Please enter a valid number for the lock minutes", true);
                }
                else
                {
                    ServerClient client = (byClientId ? FindClientByCommandInput(int.Parse(cmd[0])) : FindClientByCommandInput(cmd[0]));

                    if (client != null)
                    {
                        if (_immunePlayers.Contains(client.XUID))
                        {
                            TellClient(issuer.ClientNum, string.Format("^1[Chat Lock] ^:{0} ^7is immune to chat locks", client.Name), true);
                        }
                        else if (_playerLocks.ContainsKey(client.XUID))
                        {
                            TellClient(issuer.ClientNum, string.Format("^1[Chat Lock] ^:{0} ^7has already been chat locked", client.Name), true);
                        }
                        else
                        {
                            // Get the time span for the current date and the new date
                            TimeSpan remaining = TimeSpan.FromMinutes(timeToLock);

                            // Add the play chat lock status
                            _playerLocks.Add(client.XUID, DateTime.Now + remaining);

                            // Log the chat lock in the server locks and let the player know
                            ServerPrint(
                                string.Format(
                                    "[Chat Lock] {0} was chat locked for {1} hour(s) and {2} minute(s) by {3}",
                                    client.Name, Math.Abs(remaining.Hours), Math.Abs(remaining.Minutes), issuer.Name));

                            TellClient(issuer.ClientNum,
                                       string.Format("^1[Chat Lock] ^:{0} ^7has been chat locked successfully!",
                                                     client.Name), true);

                            TellClient(client.ClientNum,
                                       string.Format(
                                           "^1[Chat Lock] ^7You have been chat locked for {0} hour(s) and {1} minute(s)",
                                           Math.Abs(remaining.Hours), Math.Abs(remaining.Minutes)), true);
                        }
                    }
                    else
                    {
                        TellClient(issuer.ClientNum, "^1[Chat Lock] ^7No players were found", true);
                    }
                }
            }
        }

        /// <summary>
        /// Un-locks the given players chat so they can speak again
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="issuer"></param>
        /// <param name="byClientId"></param>
        private void UnlockPlayerChat(List<string> cmd, ServerClient issuer, bool byClientId)
        {
            if (cmd.Count == 0 || string.IsNullOrEmpty(cmd[0]))
            {
                TellClient(issuer.ClientNum, "^1[Chat Lock] ^7Please enter a valid player id or name", true);
            }
            else
            {
                ServerClient client = (byClientId ? FindClientByCommandInput(int.Parse(cmd[0])) : FindClientByCommandInput(cmd[0]));

                if (client != null)
                {
                    if (!_playerLocks.ContainsKey(client.XUID))
                    {
                        TellClient(issuer.ClientNum, string.Format("^1[Chat Lock] ^:{0} ^7is not chat locked", client.Name), true);
                    }
                    else
                    {
                        _playerLocks.Remove(client.XUID);

                        // Set the client messages
                        TellClient(issuer.ClientNum, string.Format("^1[Chat Lock] ^:{0} ^7is aloud to chat again", client.Name), true);
                        TellClient(client.ClientNum, "^1[Chat Lock] ^7Your chat lock is now over, be good!", true);
                    }
                }
                else
                {
                    TellClient(issuer.ClientNum, "^1[Chat Lock] ^7No players were found", true);
                }
            }
        }

        /// <summary>
        /// Creates a new thread to start listing the chat locked players, it runs on a thread so that the chat
        /// doesn't get inundated with a players information
        /// </summary>
        /// <param name="issuer"></param>
        private void FindChatLockedPlayers(ServerClient issuer)
        {
            if (_playerLocks.Count == 0)
            {
                TellClient(issuer.ClientNum, "^1[Chat Lock] ^7No players are currently chat locked", true);
            }
            else
            {
                new Thread(() => DisplayLockedPlayer(issuer)).Start();
            }
        }

        /// <summary>
        /// Displays the players name and total time remaining one by one using the previously created thread
        /// </summary>
        /// <param name="issuer"></param>
        private void DisplayLockedPlayer(ServerClient issuer)
        {
            foreach (ServerClient client in GetClients())
            {
                if (!_playerLocks.ContainsKey(client.XUID)) continue;

                // Get the time till the chat lock is over for the current player
                TimeSpan remaining = DateTime.Now - _playerLocks[client.XUID];

                TellClient(issuer.ClientNum, string.Format("^1[Chat Lock] ^:{0} ^7is locked for", client.Name), true);
                TellClient(issuer.ClientNum,
                           string.Format("^1[Chat Lock] ^:{0} ^7hour(s) ^:{1} ^7minute(s) and ^:{2} ^7second(s)",
                                         Math.Abs(remaining.Hours), Math.Abs(remaining.Minutes),
                                         Math.Abs(remaining.Seconds)), true);

                Thread.Sleep(2000);
            }
        }

        /// <summary>
        /// Attempts to find a client on the server based on the input value given
        /// </summary>
        /// <param name="findClient"></param>
        /// <returns></returns>
        private ServerClient FindClientByCommandInput(string findClient)
        {
            // Remove any whitespace from the string and set all the characters to lowercase
            findClient = findClient.ToLower().Trim();

            foreach (ServerClient client in GetClients())
            {
                string clientName = client.Name.ToLower();

                if (clientName == findClient || clientName.Contains(findClient) || client.XUID.ToLower() == findClient)
                {
                    return client;
                }
            }

            return null;
        }

        /// <summary>
        /// Attempts to find a client on the server based on the input value given
        /// </summary>
        /// <param name="findClient"></param>
        /// <returns></returns>
        private ServerClient FindClientByCommandInput(int findClient)
        {
            foreach (ServerClient client in GetClients())
            {
                if (client.ClientNum == findClient)
                {
                    return client;
                }
            }

            return null;
        }
    }
}